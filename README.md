# Wardrobify

Team: Connor & Andres

## Design

## Shoes microservice
Connor will be doing shoes microservice

BinVO model with the following properties:
- closet_name as a CharField
- bin_number as a PositiveSmallIntegerField

Shoe model with the following properties:
- manufacturer as a CharField
- model_name as a CharField
- color as a CharField
- picture_url as a URLField
- bin as a ForeignKey of BinVO

## Hats microservice
Andres will be doing hats microservice 
hats model will include:
Hat
style
color
location of what bin its in 
//
extra 

Explain your models and integration with the wardrobe
microservice, here.
