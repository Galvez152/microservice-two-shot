from django.http import JsonResponse
from common.json import ModelEncoder
from .models import BinVO, Shoe
from django.views.decorators.http import require_http_methods
import json

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "import_href",
        "closet_name",
        "bin_number",
    ]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "model_name",
    ]
    def get_extra_data(self, o):
        return {'bin_number': o.bin.bin_number, 'closet_name': o.bin.closet_name}

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
    ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            bin = BinVO.objects.get(import_href = content['bin'])
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin"}, status=400
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(shoe, encoder = ShoeDetailEncoder,
        safe = False)

@require_http_methods(["GET", "PUT", "DELETE"])
def api_shoe(request, pk):
    try:
        shoe = Shoe.objects.get(id = pk)
    except Shoe.DoesNotExist:
        return JsonResponse({'message': 'Shoe not found'}, status = 400)
    if request.method == "GET":
        return JsonResponse(
            shoe, encoder = ShoeDetailEncoder, safe = False
        )
    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id = pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            bin = BinVO.objects.get(import_href = content['bin'])
            content['bin'] = bin
        except BinVO.DoesNotExist:
            return JsonResponse({'message': 'Invalid bin'}, status = 400)
        Shoe.objects.filter(id = pk).update(**content)
        shoe = Shoe.objects.get(id = pk)
        return JsonResponse(
            shoe, encoder = ShoeDetailEncoder, safe = False
        )
