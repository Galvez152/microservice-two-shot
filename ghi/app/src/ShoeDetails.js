import { useParams } from 'react-router-dom';
import React from 'react';

function ShoeDetails() {
    const params = useParams();
    const url = `http://localhost:8080/api/shoes/${params.shoeId}`
    return (
        <div>
            {url}
        </div>
    )
}

export default ShoeDetails;