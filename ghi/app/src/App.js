import { BrowserRouter, Routes, Route , useParams} from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './ShoesList';
import ShoeForm from './ShoeForm';
import ShoeDetails from './ShoeDetails';
import HatList from './HatList';
import HatForm from './HatForm';
import GetHatDeatils from './HatDetail';
import HatDetails from './HatDetail';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes" element={<ShoesList shoes={props.shoes}/>}/>
          <Route path="shoes/details/:shoeId" element={<ShoeDetails/>}/>
          <Route path="shoes/new" element={<ShoeForm />}/>
          <Route path="hats" element={<HatList/>}>
            <Route path=":hatId"element={<HatDetails />}/>
            <Route path="hats/new" element={<HatForm />}/>
          </Route>
        </Routes>
      
      </div>
    </BrowserRouter>
  );
}

export default App;
