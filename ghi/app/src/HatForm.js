import React from 'react';

class HatForm extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            style_name:'',
            color:'',
            fabric:'',
            location:'',
            locations:[]
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange(event) {
        const newState = {};
        newState[event.target.id] = event.target.value;
        this.setState(newState);
        console.log(newState)
      }
    
 
    handleChangeLocation(event) {
        const value = event.target.value;
        this.setState({ location: value });
        
      }
    async componentDidMount() {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          this.setState({ locations: data.locations });
        }
      };

      async handleSubmit(event) {
        
        event.preventDefault();
        const data = {...this.state};
        delete data.locations;
        console.log(data)
        const locationUrl = `http://localhost:8090/api/hats/`;
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
          const newLocation = await response.json();
          console.log(newLocation);
          
          ;
        }
      }
    render() {
        return(
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new hat</h1>
                <form onSubmit={this.handleSubmit} id="create-hat-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChange} value={this.state.style_name}placeholder="Name" required type="text" name="name" id="style_name" className="form-control" />
                    <label htmlFor="name">style name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChange} value={this.state.color}placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                    <label htmlFor="name">Color</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChange} value={this.state.fabric}placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                    <label htmlFor="name">Fabric</label>
                  </div>
               
                  <div className="mb-3">
                <select onChange={this.handleChange} value={this.state.location} required className="form-select" id="location">
                  <option value="">Choose a location</option>
                  {this.state.locations.map(location => {
                    return (
                      <option key={location.href} value={location.href}>{location.closet_name}</option>
                    )
                  })}
                </select>
              </div>
                  <button  className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
          
        )
    }
}
export default HatForm