import {NavLink, Outlet} from 'react-router-dom';
import React from 'react';

function DeleteShoe() {
    console.log('deleted');
}

function ShoesList(props) {
    if (props.shoes === undefined) {
        return null;
    }
        return (
            <div>
                <table className="table table-hover">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Closet</th>
                            <th>Bin</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        {props.shoes.map(shoe => {
                            return (
                                <tr key={shoe.href}>
                                    <td>
                                        <NavLink to={`details/${shoe.id}`}>{shoe.model_name}</NavLink>
                                    </td>
                                    <td>{shoe.closet_name}</td>
                                    <td>{shoe.bin_number}</td>
                                    <td>
                                        <button onClick={() => 
                                            DeleteShoe()} 
                                            type="button" className="btn btn-outline-danger btn-sm">X</button>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
                <div>
                    <NavLink to="/shoes/new" className="btn btn-primary">Add a shoe</NavLink>
                </div>
                <Outlet/>
            </div>
        );
}

export default ShoesList;