import React from 'react';
import {Link, NavLink, Outlet}  from "react-router-dom";

class HatList extends React.Component{
  constructor(props){
    super(props);
    this.state={
      hats:[]
    }
  }
  async componentDidMount() {
    const url = 'http://localhost:8090/api/hats';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      console.log(data)
      this.setState({ hats: data.hats });
    }
  }
  render() {

 
    return (
      
      <div>
        
      <div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>location</th>
          </tr>
        </thead>
        <tbody>
        
          {this.state.hats.map(hat => {
            return (
              
              <tr key={hat.id}>
                
                <td><Link to={{pathname:`/hats/${hat.id}` }}>{ hat.style_name }</Link></td>
            
          
                <td>{ hat.location}</td>
                
              </tr>
              
            );
          })}

       </tbody>
       
      </table>
      <Outlet />
      </div>
      
      
      
     
      </div>
      
      
    
     
  
    );
    }
    }
  
        
  
export default HatList;