import React from "react";

class ShoeForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            manufacturer: '',
            modelName: '',
            color: '',
            pictureUrl: '',
            bins: [],
        };

        this.handleManufacturerChange = this.handleManufacturerChange.bind(this);
        this.handleModelChange = this.handleModelChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handlePictureChange = this.handlePictureChange.bind(this);
        this.handleBinChange = this.handleBinChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleManufacturerChange(event) {
        const value = event.target.value;
        this.setState({ manufacturer: value });
    }
    handleModelChange(event) {
        const value = event.target.value;
        this.setState({ modelName: value });
    }
    handleColorChange(event) {
        const value = event.target.value;
        this.setState({ color: value });
    }
    handlePictureChange(event) {
        const value = event.target.value;
        this.setState({ pictureUrl: value });
    }
    handleBinChange(event) {
        const value = event.target.value;
        this.setState({ bin: value });
        console.log(this.state);
    }

    async handleSubmit(event) {
        console.log('submitted')
        event.preventDefault();
        const data = {...this.state};
        data.model_name = data.modelName;
        data.picture_url = data.pictureUrl;
        delete data.modelName;
        delete data.pictureUrl;
        delete data.bins;

        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe);
            const cleared = {
                manufacturer: '',
                modelName: '',
                color: '',
                pictureUrl: '',
                bin: '',
                bins: [],
            };
            this.setState(cleared);
        }
    }

    async componentDidMount() {
        try{
            const url = 'http://localhost:8100/api/bins'
            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                this.setState({ bins: data.bins })
            } else {
                throw (new Error("Response Error", response));
            }
        } catch (err) {
            console.log(err);
        }
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-7">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new shoe</h1>
                        <form onSubmit={this.handleSubmit} id="create-shoe-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleManufacturerChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" value={this.state.manufacturer}/>
                                <label>Manufacturer</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleModelChange} placeholder="Model name" required type="text" name="model_name" id="model_name" className="form-control" value={this.state.modelName}/>
                                <label>Model name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" value={this.state.color}/>
                                <label>Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handlePictureChange} placeholder="Picture URL" required type="text" name="picture_url" id="picture_url" className="form-control" value={this.state.pictureUrl}/>
                                <label>Picture URL</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleBinChange} required id="bin" name="bin" className="form-select" value={this.state.bin_number}>
                                    <option>Select a bin</option>
                                    {this.state.bins.map(bin => {
                                        return (
                                            <option key={bin.href} value={bin.href}>
                                                {bin.closet_name} - Bin number: {bin.bin_number}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default ShoeForm;