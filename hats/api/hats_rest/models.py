from django.db import models
from django.urls import reverse
# Create your models here.
class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True,null=True)
    closet_name=models.CharField(max_length=200)


class Hat(models.Model):
    style_name=models.CharField(max_length=200)
    color=models.CharField(max_length=200)
    fabric=models.CharField(max_length=200)

    location=models.ForeignKey(LocationVO,related_name="hats",on_delete=models.CASCADE)

    def get_api_url(self):
        return reverse('api_hats', kwargs={'pk': self.pk})

    def __str__(self):
        return self.style_name

   