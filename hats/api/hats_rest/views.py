from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from django.http import JsonResponse
from .models import LocationVO,Hat
import json
class LocationVOEncoder(ModelEncoder):
    model=LocationVO
    properties=[
    "closet_name",
    "import_href"]
class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "style_name",
        "color",
        "fabric",
        "location",

    ]
    encoders = {
        "location": LocationVOEncoder(),
    }
class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "style_name",
        "id"
        
    ]
    def get_extra_data(self, o):
        return {"location": o.location.closet_name}
  
   

@require_http_methods(["GET", "POST"])
def api_list_hats(request,location_vo_id=None):
     if request.method == "GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
        else:
            hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
     else:
        content = json.loads(request.body)
        try:
            location = LocationVO.objects.get(import_href = content['location'])
            print("this is the location"+str(location))
            content["location"] = location
            
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location"}, status=400
            )
        
        
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_detail_hat(request, pk):
    try:
        hat = Hat.objects.get(id = pk)
    except Hat.DoesNotExist:
        return JsonResponse({'message': 'hat not found'}, status = 400)
    if request.method == "GET":
        return JsonResponse(
            hat, encoder = HatDetailEncoder, safe = False
        )
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id = pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            location = LocationVO.objects.get(import_href = content['location'])
            content['location'] = location
        except LocationVO.DoesNotExist:
            return JsonResponse({'message': 'Invalid location'}, status = 400)
        Hat.objects.filter(id = pk).update(**content)
        hat = Hat.objects.get(id = pk)
        return JsonResponse(
            hat, encoder = HatDetailEncoder, safe = False
        )

# Create your views here.
